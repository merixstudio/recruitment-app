import React from 'react';
import AppBar from 'material-ui/AppBar';

const Header = () => (
  <AppBar title="Merix Recrutation Quiz" className="header" showMenuIconButton={false} />
);

export default Header;
